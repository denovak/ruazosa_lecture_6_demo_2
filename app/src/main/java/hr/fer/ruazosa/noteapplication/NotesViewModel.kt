package hr.fer.ruazosa.noteapplication

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class NotesViewModel: ViewModel() {
    var repository: NotesRepository? = null
    var allNotes: MutableLiveData<List<Note>>  = MutableLiveData<List<Note>>()

    fun initializeRepository(application: Application) {
        repository = NotesRepository(application)
    }

    fun getNotes() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val notesList = repository?.noteDao?.getAll()
                withContext(Dispatchers.Main) {
                    allNotes.value = notesList
                }
            }
        }
    }
    fun insertNote(note: Note) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository?.noteDao?.insertAll(note)

            }
        }
    }

}