package hr.fer.ruazosa.noteapplication


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NotesDao {
    @Query("SELECT * FROM note")
    fun getAll(): List<Note>
    @Insert
    fun insertAll(vararg notes: Note)

}