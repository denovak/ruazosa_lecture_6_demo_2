package hr.fer.ruazosa.noteapplication


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_notes_details.*
import java.util.*

class NotesDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_details)

        val viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)

        viewModel.initializeRepository(application)

        saveNoteButton.setOnClickListener {
            var note = Note()
            note.noteId = null
            note.noteTitle = noteTitleEditText.text.toString()
            note.noteDescription = noteDescriptionEditText.text.toString()
            note.noteDate = Date()
            viewModel.insertNote(note)
            finish()
        }
    }


}
